---
date: "2019-01-23T21:30:00+01:00"
title: About ScienceR
---

Gjalt-Jorn Peters works at the Dutch Open University, where he teaches methodology and statistics, and does research into health psychology, specifically behavior change, in general and applied to nightlife-related risk behavior. He is involved in Dutch nightlife prevention project Celebrate Safe, where he is responsible for the Party Panel study. In addition, he maintains the userfriendlyscience, ufs, and behaviorchange R packages. An overview of his academic publications is available [here](https://scholar.google.nl/citations?user=Qw8JJh0AAAAJ).
