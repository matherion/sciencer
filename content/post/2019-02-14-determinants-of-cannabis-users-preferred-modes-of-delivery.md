---
title: Determinants of cannabis users' preferred modes of delivery
author: Gjalt-Jorn Peters
date: '2019-02-14'
slug: determinants-of-cannabis-users-preferred-modes-of-delivery
categories: []
tags: []
draft: true
---

https://scholar.google.nl/scholar?hl=nl&as_sdt=0%2C5&as_vis=1&q=cannabis+mode+delivery&btnG=

https://harmreductionjournal.biomedcentral.com/articles/10.1186/s12954-016-0119-9


https://scholar.google.nl/scholar?hl=nl&as_sdt=0%2C5&as_vis=1&q=cannabis+%22mode+of+delivery%22+smoking+vap*&btnG=

http://www.letfreedomgrow.com/articles/vaporization_study.pdf

https://www.sciencedirect.com/science/article/abs/pii/S0306460317300849

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4987070/
